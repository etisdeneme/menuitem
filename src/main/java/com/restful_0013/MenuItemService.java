package com.restful_0013;

import java.util.Collection;

public interface MenuItemService {
	 Collection<MenuItem> findAll();
	 MenuItem findOne(Long id);
	 MenuItem create(MenuItem item);
}
