package com.restful_0013;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMenuItemRepository extends JpaRepository<MenuItem, Long>{
	
	
}
