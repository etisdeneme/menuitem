package com.restful_0013;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuItemServiceBean implements MenuItemService{
	
	
	@Autowired
    private IMenuItemRepository mRepository;

    @Override
    public Collection<MenuItem> findAll() {
        
        Collection<MenuItem> items = mRepository.findAll();

       
        return items;
    }
    
    @Override
    
    public MenuItem findOne(Long id) {
        

       

        MenuItem item = mRepository.findOne(id);

        
        return item;
    }

    @Override
    public MenuItem create(MenuItem item) {
   
            

        MenuItem savedItem = mRepository.save(item);

        
        return savedItem;
    }

}
