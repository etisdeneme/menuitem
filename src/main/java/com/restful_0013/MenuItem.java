package com.restful_0013;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu_item")
public class MenuItem {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private long Id;

	private String name;
	@Column(name = "description")
	private String Description;
	@Column(name = "ingredients")
	private String Ingredients;
	@Column(name = "type")
	private String Type;
	@Column(name = "restaurant_name")
	private String Restaurant_name;

	public MenuItem() {
		// nop
	}

	public MenuItem(String name, long Id) {
		this.name = name;
		this.Id = Id;
	}

	public long getId() {
		return Id;
	}

	public void setId(long Id) {
		this.Id = Id;
	}

	public String getName() {
		return name;
	}

	public void setName(String title) {
		this.name = title;
	}

	public String getDescription() {
		return Description;
	}

	public String getIngredients() {
		return Ingredients;
	}

	public String getType() {
		return Type;
	}

	public String getRestaurant_name() {
		return Restaurant_name;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public void setIngredients(String ingredients) {
		Ingredients = ingredients;
	}

	public void setType(String type) {
		Type = type;
	}

	public void setRestaurant_name(String restaurant_name) {
		Restaurant_name = restaurant_name;
	}

	@Override
	public String toString() {
		return String.format("MenuItem[Id=%d, title='%s']", Id, name);
	}
}