package com.restful_0013;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Restful0013mysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(Restful0013mysqlApplication.class, args);
	}
}
