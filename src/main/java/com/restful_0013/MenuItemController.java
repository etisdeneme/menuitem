package com.restful_0013;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MenuItemController {

	@Autowired
	private MenuItemService repo;

	@RequestMapping(value = "/api/getMenuItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<MenuItem>> getMenuItems() {

		Collection<MenuItem> items = repo.findAll();

		System.out.println("itemsSize:" + items.size());

		return new ResponseEntity<Collection<MenuItem>>(items, HttpStatus.OK);

	}

	@RequestMapping(value = "/api/menuItemFind/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MenuItem> getMenuItem(@PathVariable("id") Long id) {

		MenuItem item = repo.findOne(id);
		if (item == null) {
			return new ResponseEntity<MenuItem>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<MenuItem>(item, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/menuitems", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MenuItem> createItem(@RequestBody MenuItem item) {

		MenuItem savedItem = repo.create(item);

		return new ResponseEntity<MenuItem>(savedItem, HttpStatus.CREATED);
	}

	////////////////////////////////////////////

	@RequestMapping("/menuitem/read/{id}")
	@ResponseBody
	public String readMenuItem(@PathVariable("id") long id) {
		MenuItem item;
		try {
			item = repo.findOne(id);
		} catch (Exception e) {

			return e.getMessage();
		}
		if (item == null) {
			String errorMst = "no movie found for id " + id;

			return errorMst;
		} else {
			return item.getName() + " : " + item.getId();
		}
	}

}
